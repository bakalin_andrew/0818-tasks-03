#include <stdio.h>
#include <locale.h>
#include <string.h>

#define N 5
#define MAX 256

int main()
{
	int i, j, lines = 0;
	char txt[N][MAX];
	char *p[N], *temp;

	setlocale(LC_ALL, "rus");

	printf("������� ��������� ����� (����������� %d). ������ ������ ����� �������� ��������� �����.\n", N);

	while (1)
	{
		fgets(txt[lines], MAX, stdin);
		txt[lines][strlen(txt[lines]) - 1] = 0;

		if ((*txt[lines] == 0) || (lines == N - 1))
			break;

		lines++;
	}

	if (*txt[lines] == 0)
		lines--;

	for (i = 0; i <= lines; i++)
		p[i] = txt[i];

	for (i = 0; i < lines; i++)
		for (j = 0; j < lines - i; j++)
			if (strlen(p[j]) > strlen(p[j + 1]))
			{
				temp = p[j];
				p[j] = p[j + 1];
				p[j + 1] = temp;
			}

	if (lines >= 0)
	{
		puts("��������� ���� ������ � ������� ����������� �� ����:");
		for (i = 0; i <= lines; i++)
			puts(p[i]);
	}
	else
	{
		puts("�� ����� ������ ������� �� ����.");
	}

	return 0;
}